import java.util.*;

public class XO {
	static int R, C;
	static String[][] board;
	static String turn;

	public static void main(String[] args) {

		printStart();
		board = printBorad();
		turn = fisrtPlayer();
		while (true) {
			showBoard(board);
			printTurn(turn);
			inputPosition();
			try {
				if (R > 2 || R < 0 || C < 0 || C > 2) {
					printRule();
					continue;
				} else if (!board[R][C].equals(" ")) {
					printError();
					continue;
				}
				board[R][C] = turn;
			} catch (Exception a) {
				printRule2();
				continue;
			}
			if (checkWin(board, turn)) {
				showBoard(board);
				printWin();
				break;
			}
			boolean chk = checkDraw();
			if (chk == false) {
				showBoard(board);
				printDraw();
				break;
			}
			turn = switchPlayers(turn);
		}
	}

	private static void printRule2() {
		System.out.println("Row and Column must be number");
	}

	private static void printError() {
		System.out.println("Row " + (R + 1) + " and Column " + C + " can't choose again");
	}

	private static void printRule() {
		System.out.println("Row and Column must be number 1 - 3");
	}

	private static boolean checkDraw() {
		boolean chk = false;
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				if (board[i][j] == " ") {
					chk = true;
				}
			}
		}
		return chk;
	}

	private static void printDraw() {
		System.out.println("DRAW");
	}

	private static void printWin() {
		System.out.println(turn + " : WIN");
	}

	private static String switchPlayers(String turn) {
		if (turn.equals("X")) {
			turn = "O";
		} else {
			turn = "X";
		}
		return turn;
	}

	private static void inputPosition() {
		System.out.print("Plz choose position (R,C) :");
		Scanner kb = new Scanner(System.in);
		R = Integer.parseInt(kb.next()) - 1;
		C = Integer.parseInt(kb.next()) - 1;
	}

	private static void printTurn(String turn) {
		System.out.println("Turn: " + turn);
	}

	private static String fisrtPlayer() {
		String turn = "X";
		return turn;
	}

	private static String[][] printBorad() {
		String board[][] = new String[3][3];
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				board[i][j] = " ";
			}
		}
		return board;
	}

	private static void printStart() {
		System.out.println("Start Game OX");
	}

	public static void showBoard(String board[][]) {
		System.out.println("  1 2 3 ");
		for (int i = 0; i < 3; i++) {
			System.out.print(i + 1);
			for (int j = 0; j < 3; j++) {
				System.out.print("|" + board[i][j]);
			}
			System.out.println("|");
		}
	}

	public static boolean checkWin(String board[][], String turn) {
		for (int i = 0; i < 3; i++) {
			if (board[i][0] == turn && board[i][1] == turn && board[i][2] == turn) {
				return true;
			}
			if (board[0][i] == turn && board[1][i] == turn && board[2][i] == turn) {
				return true;
			}
		}
		if (board[0][0] == turn && board[1][1] == turn && board[2][2] == turn) {
			return true;
		}
		if (board[0][2] == turn && board[1][1] == turn && board[2][0] == turn) {
			return true;
		}
		return false;
	}
}